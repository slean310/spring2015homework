/* 
 * Steven Leandro
 * 2503241
 * 3/3/2015
 * HW:1
 * Problem 6
 * I certify this is my own work or code
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
 int main(int argc, char** argv) {

    string userInput;
            
    cout << "Hello my name is Rick. " << endl;
    cout << "Whats your name? "
            << endl;
    
    cin >> userInput;
    
    cout << "Hello "
           << userInput
           << " nice to meet you.";
    
    return 0;
 }

