/* 
 * Steven Leandro
 * 2503241 
 * 3/3/2015
 * HW:1
 * Problem: 7
 * I certify this is my own work or code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << " **************************************************"
            << endl;
    cout << "        C C C               S S S S         !!" << endl;
    cout << "      C       C           S         S       !!" << endl;
    cout << "     C                   S                  !!" << endl;
    cout << "    C                     S                 !!" << endl;
    cout << "    C                      S S S S          !!" << endl;
    cout << "    C                               S       !!" << endl;
    cout << "     C                               S      !!" << endl;
    cout << "      C       C           S         S         " << endl;
    cout << "        C C C               S S S S         00" << endl;
    cout << "***************************************************"
            << endl << endl << endl;
    cout << "     Computer Science is Cool Stuff!!!";
    
    return 0;
}

