/* 
 * Steven Leandro
 * 2503241
 * 3/3/2015
 * HW: 1
 * Problem: 5
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int int1, int2, intsum, intprod;
    cout << "Enter two integers. ";
    cin >> int1 >> int2;
    cout << endl;
    intsum = int1 + int2;
    cout << "The sum of " 
            << int1 << " and "
            << int2 << " equals " 
            << intsum << "."
            <<endl;
    
    intprod = int1 * int2;
    cout << "The product of "
            << int1 << " and "
            << int2 << " equals "
            << intprod << "."
            << endl;
    
    return 0;
}

