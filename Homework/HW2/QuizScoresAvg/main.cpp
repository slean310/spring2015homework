/* 
 * File:   main.cpp
 * Author: Steven
 *
 * Created on March 9, 2015, 8:58 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int a1, b1, c1 ,d1 , a2, b2, c2, d2, a3, b3, c3, d3;
    a1 = 9, b1 = 10, c1 = 8, d1 = 9, a2 = 7, b2 = 8;
    c2 = 9, d2 = 7, a3 = 10, b3 = 8, c3 = 8, d3 = 10;
    cout << left
            << setw(15) << "Name"
            << setw(10) << "Quiz 1"
            << setw(10) << "Quiz 2"
            << setw(10) << "Quiz 3"
            << setw(10) << "Quiz 4"
            << endl << endl;
    
    cout << setw(15) << "John"
            << setw(4) << right << a1
            << setw(6) << " "
            << setw(4) << b1
            << setw(6) << " "
            << setw(4) << c1
            << setw(6) << " "
            << setw(4) << d1
            << endl;
    
    cout << setw(15) << left << "Mary"
            << setw(4) << right << a2
            << setw(6) << " "
            << setw(4) << b2
            << setw(6) << " "
            << setw(4) << c2
            << setw(6) << " "
            << setw(4) << d2
            << endl;
    cout << setw(15) << left << "Matthew"
            << setw(4) << right << a3
            << setw(6) << " "
            << setw(4) << b3
            << setw(6) << " "
            << setw(4) << c3
            << setw(6) << " "
            << setw(4) << d3
            << endl;
    double avg1 = static_cast<double> (a1 + a2 + a3)/ 3.0;
    double avg2 = static_cast<double> (b1 + b2 + b3)/ 3.0;
    double avg3 = static_cast<double> (c1 + c2 + c3)/ 3.0;
    double avg4 = static_cast<double> (d1 + d2 + d3)/ 3.0;
    
    cout << fixed << setprecision(2);
    cout << setw(15) << left << "Average"
            << setw(4) << right << avg1
            << setw(6) << " "
            << setw(4) << avg2
            << setw(6) << " "
            << setw(4) << avg3
            << setw(6) << " "
            << setw(4) << avg4;
    
    return 0;
}

