/* 
 * File:   main.cpp
 * Author: Steven
 *
 * Created on February 23, 2015, 4:35 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string helloMyNameIsHall;
    
    helloMyNameIsHall = "Hello my name is Hal!";
    
    cout << helloMyNameIsHall <<endl;
    
    string userInput;
    
    cout << "What is your name? " << endl;
    
    cin >> userInput;
    
    cout << "Hello, " << userInput << ". I am glad to meet you. ";
    
    return 0;
}

