/* 
 * File:   main.cpp
 * Author: Steven
 *
 * Created on March 2, 2015, 4:07 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string testScores;
    
    testScores = " Put in 3 test scores. ";
    
    cout << testScores;
    
    double a, b, c;
    
    cout << " ( ";
    
    cin >> a >> b >> c;
    
    cout << " ) ";
    
    double avg = static_cast<double>(a+b+c) / 3.0;
    
    cout << endl << " The average is: "
            << avg
            << endl;
    
    return 0;
}

