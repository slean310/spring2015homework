/* 
 * File:   main.cpp
 * Author: Steven
 *
 * Created on February 25, 2015, 4:28 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string askForMeter;
    
    askForMeter = " Write a measurement in meter's to convert to miles,"
            "inches, and feet. ";
    
    cout << askForMeter << endl;
    
    int userInput;
    
    cout << " Meters: ";
    
    cin >> userInput;
    
    double met = userInput, mil = 1609.344, mm = met * mil;
    
    cout << " Meters to Miles: " << mm << endl;
    
    double ft = 3.281, mf = met * ft;
    
    cout << " Meters to Feet: " << mf << endl;
    
    double inch = mf * 12;
    
    cout << " Meters to Inches: " << inch;
    
    return 0;
}

