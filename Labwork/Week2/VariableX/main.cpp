/* 
 * File:   main.cpp
 * Author: Steven
 *
 * Created on March 2, 2015, 2:55 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    string inputVariable;
    
    inputVariable = " Type in variable x. ";
    
    cout << inputVariable << endl;
    
     double userInput;
    
    cin >> userInput;
    
    cout << endl << " x with an increment of 5 is: ";
    
    double store = userInput + 5;
    
    cout << store;
    
    return 0;
}

